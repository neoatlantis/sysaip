SYSAIP
======

System of Automatic Intelligence Processing. A system that:

* automatically collects(monitors) events;
* examine the reliability of such reports;
* announce and/or push alarms.

Requirements
------------

* `requests`
