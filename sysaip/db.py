"""
Provides a framework for database accesses.

Database will be stored at ~/.sysaip/
"""

import os
import shelve
import sys

DBPATH = os.path.join(os.path.expanduser('~'), '.sysaip')
if not os.path.isdir(DBPATH): os.system('mkdir -p %s' % DBPATH)

def getModuleDatabase():
    global DBPATH
    name = os.path.basename(os.path.realpath(os.path.dirname(sys.argv[0])))
    filename = "db-%s" % name.encode('hex') 
    fullpath = os.path.join(DBPATH, filename)
    return shelve.open(fullpath)
