import csv
import requests
import datetime
import dateutil.parser
import time
import pickle


class Earthquake:

    def _strptime(self, s):
        return dateutil.parser.parse(s)

    def __init__(self, init):
        if type(init) == str:
            init = pickle.loads(init)
        if type(init) != dict:
            raise Exception()
        # {'rms': '0.11', 'updated': '2016-01-06T23:58:35.699Z',
        #  'type': 'earthquake', 'magType': 'Md', 'longitude': '-65.1887',
        #  'gap': '288', 'depth': '38', 'dmin': '0.77255114', 'mag': '3',
        #  'time': '2016-01-06T22:27:48.600Z', 'latitude': '19.0958',
        #  'place': '87km NNW of Charlotte Amalie, U.S. Virgin Islands',
        #  'net': 'pr', 'nst': '6', 'id': 'pr16006042'
        # }
        self.id = init['id']
        self.location = (\
            float(init['latitude']),
            float(init['longitude']), 
            float(init['depth'])
        )
        self.locationDescription = init['place']
        self.magnitude = float(init['mag'])
        self.stationsReported = int(init['nst'] or 1)
        self.time = self._strptime(init['time'])
        self.updated = self._strptime(init['time'])
        self._origin = init

    def __repr__(self):
        str2time = lambda dt: dt.strftime('%c %z')
        ret =  '<Earthquake of magnitude %3.1f\n' % self.magnitude
        ret += ' reported     : %s\n' % str2time(self.time)
        ret += ' last updated : %s\n' % str2time(self.updated)
        ret += ' location     : Lat=%9.4f Lng=%9.4f Depth=%5.1fkm\n' % \
            self.location
        ret += '                (%s)\n' % self.locationDescription
        ret += '>'
        return ret

    def __eq__(self, other):
        if not isinstance(other, Earthquake): return False
        return other.id and other.id == self.id

    def __str__(self):
        return pickle.dumps(self._origin)

##############################################################################

_URL = 'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/'
URL_1h = _URL + '2.5_hour.csv'
URL_1d = _URL + '2.5_day.csv'

class USGSDownloader:

    def __init__(self, oldness):
        if oldness < 3600:
            self.__url = URL_1h
        else:
            self.__url = URL_1d

    def _parse(self, csvList):
        csvReader = csv.reader(csvList)
        rawResult = list(csvReader)
        header = rawResult[0]
        rest = rawResult[1:]
        ret = []
        for line in rest:
            if not line: continue
            if len(line) != len(header): continue
            obj = {}
            for i in xrange(0, len(line)):
                obj[header[i]] = line[i]
            ret.append(obj)
        return ret

    def work(self):
        r = requests.get(self.__url)
        if 200 != r.status_code:
            return None
        parsed = self._parse(r.text.split('\n'))
        return [Earthquake(i) for i in parsed]

if __name__ == '__main__':
    test = USGSDownloader(111101)
    e =  test.work()[0]

    stre = str(e)
    print stre

    f = Earthquake(stre)
    print repr(f)
