#!/usr/bin/env python

import time
import datetime
import sysaip.db

from _downloader import USGSDownloader


dbMod = sysaip.db.getModuleDatabase()
datetimeNow = datetime.datetime.utcnow()
timeNow = time.time()

# ---- get last successful retrieval timestamp
lastRetrieveTime = 0
try:
    lastRetrieveTime = int(dbMod['agent.lastRetrieve'])
except:
    dbMod['agent.lastRetrieve'] = 0

# ---- decide which source of earthquake data we should use
oldness = timeNow - lastRetrieveTime

# ---- downloader initialization
usgsDownloader = USGSDownloader(oldness)
usgsEarthquakes = usgsDownloader.work()
if usgsEarthquakes:
    count = 0
    for event in usgsEarthquakes:
        eventID = event.id
        eventStorageID = 'event-%s' % eventID
        if dbMod.has_key(eventStorageID): continue
        dbMod[eventStorageID] = {
            'reportFiled': False,
            'eventTime': event.updated,
            'registerTime': datetimeNow,
            'raw': str(event),
        }
        count += 1
    print "Earthquake: %d out of %d events registered." % (\
        count,
        len(usgsEarthquakes)
    )

dbMod.close()
